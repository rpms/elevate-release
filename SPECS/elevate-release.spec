Summary: ELevate project packages from the AlmaLinux Migration SIG repository
Name: elevate-release
Version: 1.0
Release: 2%{?dist}
License: ASL 2.0
URL: https://wiki.almalinux.org/sigs/Migration
Source0: RPM-GPG-KEY-ELevate
BuildArch: noarch

Provides: elevate-release = 1

%description
yum configuration for ELevate project packages from the AlmaLinux Migration SIG.

%prep
cat <<EOF > ELevate.repo
# ELevate project repo for el%{rhel}

[elevate]
name=ELevate
baseurl=https://repo.almalinux.org/elevate/el%{rhel}/\$basearch/
gpgcheck=1
enabled=1
priority=90
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ELevate

## Sources
[elevate-source]
name=name=ELevate - Source
baseurl=https://repo.almalinux.org/elevate/el%{rhel}/SRPMS/
gpgcheck=1
enabled=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ELevate
EOF

%install
install -D -m 644 %{SOURCE0} %{buildroot}%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-ELevate
install -D -m 644 ELevate.repo %{buildroot}%{_sysconfdir}/yum.repos.d/ELevate.repo

%files
%defattr(-,root,root)
%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-ELevate
%config(noreplace) %{_sysconfdir}/yum.repos.d/ELevate.repo

%changelog
* Mon Mar 14 2023 Andrew Lukoshko <alukoshko@almalinux.org> - 1.0-2
- Updated RPM-GPG-KEY-ELevate to match EL9 requirements
- Added support for EL8

* Tue Sep 28 2021 Andrew Lukoshko <alukoshko@almalinux.org> - 1.0-1
- Initial version
